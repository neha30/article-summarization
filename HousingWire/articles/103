WaPo: Stop trying to resurrect Fannie and FreddieDespite starting off as a long-shot idea, the call for the recapitalization of Fannie Mae and Freddie Mac now comes from nearly every side.

It’s still nearly because there’s at least one journal publication that adamantly says this Fannie-Freddie resurrection needs to die.

A piece in the Washington Post by its Editorial Board that states that “the persistent non-solution to the zombie-like status of Fannie Mae and Freddie Mac known as ‘recap and release’” needs to stop.

The article asserts that Washington is where good ideas go to die, while some bad ideas stick around for the long haul. A perfect example of this is the recap and release of Fannie Mae and Freddie Mac.

The plan is to return the two mortgage-finance giants to their pre-financial-crisis status as privately owned but “government-sponsored” enterprises. That is to say, to recreate the private-gain, public-risk conflict that helped sink them in the first place. Their income would recapitalize the entities, rather than be funneled to the treasury, as is currently the case. Then they could exit the regulatory control known as “conservatorship” that has constrained them since 2008 — and resume bundling home loans and selling them, as if it had never been necessary to bail them out to the tune of $187 billion in the first place.

This bad idea’s undeadness illustrates the risks inherent in perpetuating the institutional limbo around the U.S. housing finance system, which is, unfortunately, what Congress and the administration have done so far.

Even with this strong dissent toward the recap and release of the GSEs, there are still plenty of people on board with the idea. Community lenders, affordable housing advocates, civil rights groups, interested observers, financial analysts and others have called all for a change in governmental policy that would enable Fannie Mae and Freddie Mac to rebuild a capital base.