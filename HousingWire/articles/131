Jobs report shockingly lower than economists suspectedWhereas the unemployment rate declined by 0.3% to 4.7% to 7.4 million in May, nonfarm payroll employment changed very little with an increase of 38,000, according to the U.S. Bureau of Labor Statistics.

This number is much lower than the anticipated 160,000. Although there was speculation that the Verizon strike would decrease that number because it was not factored in, that would have dropped the estimates to 120,000, still much higher than the actual increase.

When the official NFP employment figures are released on Friday, the number will be closer to the estimated 120,000 increase, Chief U.S. Economist Paul Ashworth stated. The current estimate of 162,000 is due to forecasters not including the strike.

He was slightly off.

“The labor force participation rate seems to be heading back towards the lows of last September, which puts the number back towards lows not seen since the 1970s,” said Brent Nyitray, director of capital markets for iServe Residential Lending, a multi-state residential mortgage banker.

“The population increased by 205,000 while the labor force fell by 458,000,” Nyitray said. “The unemployment rate fell to 4.7%, however that is due to half a million people exiting the labor force.”

So what does that mean for interest rate hike that was on the horizon? It doesn’t look like that’s coming any time soon, according to an article by Stephen Alpher, news editor for Seeking Alpha.

Many economists were confused by the low gains in the labor force.

“Today’s May jobs report fell well short of already low expectations,” Fannie Mae Chief Economist Doug Duncan said. “Even after accounting for the striking Verizon workers, private job gains still remain at a paltry 60,000, a puzzling downshift from their recent trend.”

And that wasn’t the only bad news in today’s jobs report. Construction labor also saw unusually low numbers.

“News was not good for real estate either, as construction employment posted the biggest loss in May since the end of 2013, and the small gain in April was revised to a modest loss,” Duncan said.

“Today’s jobs report heightens uncertainty surrounding the uneven performance of the domestic economy amid downside risks abroad,” Duncan continued. “Encouraging signs seen in consumer spending and home sales at the start of the second quarter are now tempered by what appears to be a significant loss of momentum in the labor market.”