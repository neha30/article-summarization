Minorities lose out as banks favor Jumbo mortgagesJumbo loans have become a new favorite among banks as companies seek to minimize risk with the high dollar mortgages, according to an article by Rachel Ensign, Paul Overberg and Annamaria Andriotis for The Wall Street Journal. The problem? Minorities are losing out.

Jumbo loans, or loans above 417,000 in most markets, are becoming a greater focus for banks, according to the article. Jumbo loans at JPMorgan Chase, for example, were at 22.5% in 2014, up from 9.1% in 2007. Consequently, its mortgages to blacks fell from 8.2% to 3.8%, and to Hispanics from 15.1% to 8.7%.

These [Jumbo] loans predominantly go to white and Asian borrowers, the analysis showed. In 2014, 3.0% of the biggest banks’ jumbo borrowers were Hispanic and 1.3% were black. As the 10 big banks issued proportionally more jumbos, they collectively decreased their share of all home loans to blacks and Hispanics. Their proportion of lending to those minorities also fell in non-jumbo mortgages alone, though not by as much. Among all approved mortgage applicants from the 10 banks, 5.3% were black in 2014, down from 7.8% in 2007; 7.4% were Hispanic, down from 10.6%.

That being said, banks must tread carefully to ensure they are not violating fair-lending regulations.

There is little way of definitively knowing if a bank’s lending will trigger fair-lending violations, said Mr. Thomas, the fair-lending consultant. To avoid trouble, he said, banks must not egregiously ignore minority or low-income borrowers even as they focus on jumbos.

Back in 2015, for example, Hudson City Bancorp agreed to settle for $33 million outside of court, without admitting wrongdoing over allegations of withholding mortgages from minorities, according to an article by Andriotis and Ensign for The WSJ.