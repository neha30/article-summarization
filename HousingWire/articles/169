Competition about to be the next top homebuyer concern?Affordability, while the number one homebuyer concern, might soon be unseated by worrying about buyer competition, according to a new housing report from Redfin.

In roughly 7 months, homebuyer concerns about competition nearly doubled, nearly 19% cited competition from other buyers as their top worry in May, up from 16% in February and 11% in November.

Redfin based its results on a survey of 975 homebuyers conducted this month.

“Though enticed by high rents and low mortgage rates to begin a home search, first-time buyers face a number of obstacles in today’s competitive market,” said Redfin chief economist Nela Richardson. “In many cities, starter homes have seen the largest price increases because the supply of affordable homes on the market is so low and the demand for these homes is so high.”

The rising concern isn’t too much of a surprise given that 60% of Redfin offers have faced competition in May to date and over half of all offers faced competition for much of each of the last four years.

This chart shows the break down of what concerns homebuyers the most.

Going back to affordability, the concern has remained steady over the past several quarters, while inventory, which came in third, and affordability posted noticeable variations.

Affordability problems, however, have caught the attention of industry, with three of the big banks announcing a 3% down mortgage this year.

Earlier this year, Bank of America announced plans to begin offering a 3% down mortgage lending program that did not involve the Federal Housing Administration, important considering how many lenders have recently run afoul of the federal government for the participation in FHA lending.

In late May, Wells Fargo rolled out its own 3% down lending program, partnering with Fannie Mae and Self-Help, with the aim of offering consumers lower out-of-pocket costs, expanding the lender’s credit criteria and pushing homebuyer education to help more first-time homebuyers and low- to moderate-income families achieve “sustainable homeownership.”

JPMorgan announced a 3% down mortgage in late May as well, giving prospective homebuyers, especially first-time homebuyers who are struggling to save up for a down payment, have a new, and significant, outlet that they can now turn to.