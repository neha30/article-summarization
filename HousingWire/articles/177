Wells Fargo now offers 3% down payment mortgagesWells Fargo announced on Thursday that it now offers a down payment of as little as 3% for fixed-rate mortgages, answering calls in the industry to expand the credit box.

The new low-down payment program, yourFirst Mortgage, which went live on Monday, offers lower out-of-pocket costs, expands credit criteria and pushes homebuyer education to help more first-time homebuyers and low- to moderate-income families achieve sustainable homeownership.

“With yourFirst Mortgage, we wanted to provide access to credit and simplify the experience while maintaining responsible lending practices. We partnered with credit experts such as Fannie Mae and Self-Help, an affiliate of the Center for Responsible Lending, to develop an easy-to-understand affordable loan option that gives homebuyers the best offering in the market,” said Brad Blackwell, executive vice president with Wells Fargo Home Lending.

Here's a tweet from Blackwell on the new product

If there was one word that summed up why this program is different for Wells Fargo, it would be simplicity. Greg Gwizdz, executive vice president of national retail sales with Wells Fargo, said in an interview, “One of the cornerstones of the product is simplicity. The complexities create barriers for a lot of homebuyers because they really have a hard time understanding what they need to do to qualify.”

For example, to simplify the process, mortgage insurance can either be rolled in to the cost of the loan or purchased separately by the borrower.

One key factor in the new program that makes it different than the rest of the market is that it only requires a 620 FICO score.

However, Gwizdz emphasized that while the FICO is only 620, people shouldn’t get hung up on that one element.

“We feel we have a strong balance of risk and opportunity for homeownership. We looked at all the FICO information, and 620 struck the right balance. We believe consumers with 620 and over combined with education and the ability to repay has strong performance. It’s really the combination,” he said.

Back in February, Bank of America also announced a program with as little as 3% down that requires no mortgage insurance. While Bank of America also partnered with Self-Help Ventures Fund, it partnered with Freddie Mac rather than Fannie Mae.

Key factors in Bank of America’s program are that the three companies are working together to originate and back the loans. This means that Self-Help will buy the loans and servicing rights, along with taking the first-loss position. Freddie Mac will then purchase all of the eligible affordable mortgages originated via the Self-Help and Bank of America partnership.

The Bank of America program also requires a minimum FICO score of 660, and first-time buyers must participate in homebuyer education.

This is not the model that Wells Fargo is using, rather the bank worked with Fannie and Self-Help to better understand borrowers and first-time homebuyers.

Gwizdz said that the mortgages in this program will go through Wells Fargo’s capital markets team after origination just like any other program.

Also, while Wells Fargo does not require homebuyer education, it encourages it and gives customers who have a down payment of less than 10% the opportunity to earn a 1/8 percent interest rate reduction when they complete a homebuyer education course.

Gwizdz added, “Wells Fargo historically has put a lot of effort put into sustainable homeownership, and it’s been very clear to use over the last couple years that one of the barriers has been the down payment.”

JPMorgan announced a 3% down mortgage this week as well, giving prospective homebuyers, especially first-time homebuyers who are struggling to save up for a down payment, have a new, and significant, outlet that they can now turn to.