How tighter lending conditions affected economic slow-downThe second half of 2015 experienced tighter lending conditions and, as a result, economic growth within the U.S. slowed, according to a Liberty Street blog by Marco Del Negro, Marc Giannoni and Micah Smith, analysts for the Federal Reserve Bank of New York.

Using a model called the FRBNY DSGE model, the bank determined that the quantifiable economic slowdown would be much greater if the Federal Reserve didn't delay liftoff in the federal funds rate.

This chart shows that the evolution in credit conditions did not follow the forecast put forth by the FRBNY DSGE model.

Increases in credit spreads tend to be associated with subsequent slowdowns in economic activity, with the Great Recession being a salient example. In part, such increases reflect investors’ concerns about future economic conditions, changes in firms’ leverage, heightened worries about borrowers’ default, and so on. However, as Simon Gilchrist and Egon Zakrajšek and others have shown in their research, such increases in credit spreads often cause an economic slowdown. A natural question is then: Did the rise in credit spreads reflect deteriorating economic conditions or did the causality run the other way around in this episode?

The FRBNY DSGE model seeks to sort through some of the different channels in order to show more accuracy. The model combines prior information about parameters with important macroeconomic data.

How restrictive, then, has this rise in credit spreads been on the U.S. economy? The left panel in the next chart shows actual growth, from 2013:Q1 to 2016:Q1, in blue. After peaking at 3.9 percent (annual rate) in 2015:Q2, GDP growth slowed markedly for the subsequent three quarters, reaching a paltry 0.5 percent in 2016:Q1. Part of the slowdown was anticipated as of mid-2015: The red line, which indicates the 2015:Q2 forecast, shows that growth was expected to return to just below 2 percent. It just ended up being significantly lower.

For more details from the Federal Reserve Bank of New York on how the tightening on of financial conditions caused a significant slowdown in growth, and how delaying the federal funds rate liftoff offset part of the adverse shocks, click here.