[charts] The rise of the million-dollar home in today's hottest marketsUnlike the rest of America’s million dollar homes that only witnessed a slight increase in price, there are niche markets where seven figure homes are now normal, particularly in the nation’s hottest housing markets.

According to a recent Trulia blog, since 2012 the share of million dollar homes in the United States has increased from 1.6% to 3%, but many metros and neighborhoods are seeing a much larger increase.

To no surprise, among the 100 largest metros, San Francisco experienced the largest increase in the share of million dollar homes in the country, growing to 57.4% in 2016 from 19.6% of homes in 2012.

Following close behind were San Jose and Oakland in second and third, respectively.

And the rise in housing prices isn’t exclusive to the San Francisco Bay Area.

Trulia noted that metros in Southern California, Hawaii and the Northeast also posted noticeable gains, having nearly doubled the share of million dollar homes in just four years.

Here’s a full chart showing the top 10 housing markets with the biggest increases in million dollar homes:

Overall, the sales price for the luxury housing market, defined as the top 5% of homes sold in a given quarter, decreased, according to a recent report by Redfin. Prices decreased by about 1.1% annually in the first quarter of 2016, the report stated.