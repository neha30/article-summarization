Redfin: New listings decreased for first time in two yearsNew listings fell nationwide in April by 1.1% annually, marking the first drop since August of 2014, according to real estate brokerage Redfin.

Home sales increased 2.5% in April and inventory fell for the seventh consecutive month, dropping by 3.2%. This was not an isolated finding, with since two out of three markets Redfin tracks showed a drop in listings compared to last year. The largest declines happened in the Northeast with Boston, New York and Philadelphia seeing annual declines of over 10%.

“Trade-up buyers seem to be losing their mojo heading into the heart of the spring selling season,” Redfin chief economist Nela Richardson said. “Repeat buyers tend to list early because they are most often also looking for another home to buy in the near future.”

“A slowdown in new listings reflects a lack of confidence on the part of the homeowner that they can find a desirable home to purchase,” Richardson said. “This triggers a domino effect down the supply chain that leads to lower sales in tight markets. West Coast cities like Denver, Portland and Seattle were prime examples of this in April.”

Experts explain that what was presumed to be the hottest buying season of the year isn’t shaping up to that due to a flurry of factors that are growing into the "perfect storm" to hinder housing, explained Kevin Golden, director of analytics with a la mode, at the Mortgage Bankers Association’s Secondary conference in New York City.

Read all the Redfin findings here.