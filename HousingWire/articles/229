Strong housing market helps reduce lingering foreclosure inventoryA little less than 20k residential properties in the foreclosure process lie vacant (zombies), representing 4.7% of all foreclosures, according to the Q2 2016 U.S. Residential Property Vacancy and Zombie Foreclosure Report by housing data provider RealtyTrac.

Currently, 19,187 properties are undergoing zombie foreclosures, a decrease of 3.1% from last month.

RealtyTrac finds these numbers by matching its address-level property data for nearly 85 million U.S. residential properties in metropolitan areas with at least 100,000 residential properties against monthly updated data from the U.S. Postal Service indicating whether a property has been flagged as vacant.

“Lenders have been taking advantage of the strong seller’s market to dispose of lingering foreclosure inventory over the past year, evidenced by 12 consecutive months of increasing bank repossessions ending in February and now evidenced by these numbers showing a sharp drop in vacant zombie foreclosures compared to a year ago,” RealtyTrac senior vice president Daren Blomquist said.

“As these zombie foreclosures hit the market for sale they are providing a modicum of relief for the pressure cooker of escalating prices and deteriorating affordability that have defined the U.S. housing market in recent years,” Blomquist said.

These are the states with the most zombie foreclosures:

“While overall foreclosure activity has declined from last year, we have experienced a slight increase in vacancies of residential properties facing foreclosure,” said Michael Mahon, president at HER Realtors, from the Cincinnati, Dayton and Columbus markets in Ohio.

“As market supply and availability has remained low in many areas of the state, loan servicing companies have stepped up efforts of addressing homeowners delinquent on their mortgages, and have accelerated the process of filing for foreclosure,” Mahon said. “Feeling the pressure of loan servicers, many homeowners give up hope early, thus creating the vacancy event.”

Among states with at lease 100 zombie foreclosures, these are the states with the highest zombie foreclosure rates: