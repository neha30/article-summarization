Freddie Mac: 30-year mortgage rate falls to lowest level in 3 yearsContinuing the trend for most of the year, mortgage rates dropped again, falling to their lowest point in three years.

This week's decline comes courtesy of disappointing April employment data, Freddie Mac’s Primary Mortgage Market Survey showed.

This marks the third consecutive week of declines, with mortgage rates dropping for much of the year.

“Disappointing April employment data once again kept a lid on Treasury yields, which have struggled to stay above 1.8% since late March. As a result, the 30-year mortgage rate fell 4 basis points to 3.57%, a new low for 2016 and the lowest mark in 3 years,” said Sean Becketti, chief economist with Freddie Mac.

“Prospective homebuyers will continue to take advantage of a falling rate environment that has seen mortgage rates drop in 14 of the previous 19 weeks,” he added.

The U.S. Bureau of Labor Statistics reported that total nonfarm payroll employment increased by 160,000 in April as market growth started to slow down. This is down from March’s growth of 215,000 jobs.

The 30-year fixed-rate mortgage averaged 3.57% for the week ending May 12, 2016, down from last week’s 3.61%. A year ago at this time, the 30-year FRM averaged 3.85%.

Also dropping, the 15-year FRM came in at 2.81%, falling from 2.86% last week. A year ago at this time, the 15-year FRM averaged 3.07%.

The 5-year Treasury-indexed hybrid adjustable-rate mortgage averaged 2.78%, decreasing from 2.80% a week ago. A year ago, the 5-year ARM averaged 2.89%.

The chart below shows  just how low mortgage rates are now.