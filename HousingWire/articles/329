Texas housing inventory officially at an all-time lowIf the Lone Star state wasn’t already struggling enough with today’s tight market, the news just got worse for home shoppers as current housing supply sits at an all-time low, a problem that now impedes the entire state.

According to the 2016-Q1 Texas Quarterly Housing Report released by the Texas Association of Realtors, housing inventory fell to an all-time low of 2.8 months in 2016-Q1, a decline of 0.6 months from the first quarter of 2015.

To help put into perspective just how dire the situation is, the Real Estate Center at Texas A&M University estimates that a monthly housing inventory between 6.0 and 6.5 months is a level at which the supply and demand for homes are balanced. This is about 47% to 43% less than where is needs to be in order to be balanced.

And on top of this, active listings fell sharply in the first quarter of this year, dropping 11.9% year-over-year to 74,276 active listings.

Texas homes also continued to spend less time on the market, spending an average of 64 days on the market, a decrease of three days compared to the same quarter of the prior year.

“Housing inventory remains extremely limited in Texas. Low housing inventory combined with rising property values is making housing affordability a challenge, not just in Texas’s metro areas but across the state. This could become a larger problem if there is not greater balance between supply and demand in the future,” said Leslie Rouda Smith, chairman of the Texas Association of Realtors.

Smith remains optimistic about the state saying, “Despite the economic downturn in some parts of the state, home sales continue to be strong, indicating the enduring demand of Texas real estate. Our state continues to be a hub for relocation activity, business development and job growth.”

Texas home sales jumped in the first quarter of 2016. According to the report, 65,265 homes were sold in Texas in the first quarter of 2016, a 7.8% increase from the same quarter of 2015. Home prices also grew, with the median price for Texas homes increasing 5.4% year-over-year to $195,000.

Texas’ population is only estimated to keep getting bigger, with the state overtaking the list of the fastest-growing cities in America.

According to data from the U.S. Census Bureau, Texas’ two largest metro areas gained more residents from July 1, 2014, to July 1, 2015 than any other metro area in the country.

The latest Census Bureau population estimates show that the Houston-The Woodlands-Sugar Land metro area grew by 159,00 residents from July 1, 2014, to July 1, 2015, while the Dallas-Fort Worth-Arlington metro area grew by 145,000 residents.

“The Texas economy is experiencing a cooling-off period after a five-year boom, so the Texas housing market’s strong gains despite the current uneasiness in the state economy are remarkable,” said Jim Gaines, economist with the Real Estate Center at Texas A&M University.

“It will be interesting to see how Texas real estate activity performs in the next two quarters, typically the strongest quarters for home sales every year,” said Gaines. “That performance will show the full strength of the Texas housing market in 2016.”