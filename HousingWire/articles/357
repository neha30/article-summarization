Ocwen responds to National Mortgage Settlement foreclosure holdsEarly Thursday morning, Joseph Smith, the monitor of the National Mortgage Settlement, announced that Ocwen Financial was not in compliance with one of the performance metrics of the National Mortgage Settlement and prohibited the nonbank from taking foreclosure actions on more than 17,000 loans.

According to Smith’s office, Ocwen “was delayed” in implementing its Corrective Action Plan for the failure of Metric 31, which relates to the mortgage servicer sending a loan denial motivation to a borrower, because of “difficulties in resolving the technical issues that led to the original fail.”

Smith’s office said that because of those issues Ocwen must place 17,496 loans that "could have been affected" by this issue on foreclosure hold.

“While Ocwen has made progress toward correcting a number of past fails, it has not resolved its issues that led to its failure of Metric 31,” Smith said. “Therefore, I will not allow Ocwen to move forward with foreclosures on any borrowers who could have been affected by this failure until each of these borrowers has correct information and a chance to appeal.”

In a lengthy response published Thursday afternoon, Ocwen responded to Smith’s office and the nature of the sanctions that Smith’s office placed on it.

Ocwen notes that the issue at hand originally occurred in the third quarter of 2014.

“Ocwen takes borrower harm very seriously and worked with Office of Mortgage Settlement Oversight to place certain loans on a hold to ensure that no foreclosure sale would take place until OMSO reviewed and validated that all matters associated with Metric 31 were resolved,” the company said in a statement.

“The Monitor’s report today further noted it has approved the corrective action plan for Metric 31, and that Ocwen reported completing all implementation of that plan as of March 8, 2016,” Ocwen continued.

“These holds are not ‘frozen foreclosures’ but rather an agreement not to foreclose until OMSO reviewed and approved Ocwen’s remediation,” Ocwen continued. “Many of these loans have never been referred to foreclosure and never will be. The Company has already resumed internal testing of Metric 31, and expects future OMSO reports will reflect that its concerns are resolved.”

According to Ocwen, it referred over 19,000 loans to foreclosure and completed approximately 7,000 foreclosures in the first quarter of 2016, while completing more than 16,600 loan modifications.

Ocwen also noted Smith’s office stated that the company passed all the metrics that were tested in the first and second quarters of 2015, and said that that the company has implemented the “appropriate actions” to be compliant with obligations under the NMS.

“Ocwen is pleased that the Monitor’s report determined that we have passed all tested metrics in the first and second quarters of 2015,” Ocwen’s president and CEO, Ron Faris, said.

Ocwen also noted that Smith’s office stated that it has fulfills its consumer relief obligation under the NMS, by providing more than $2 billion in consumer relief.

“The Monitor’s latest Consumer Relief Report is another positive step for Ocwen, and confirms our commitment to providing real solutions to struggling homeowners,” Faris said.

“Our work with distressed borrowers will not end just because we have exceeded our NMS obligations,” Faris continued. “Families across the country are still being impacted by the financial crisis. Ocwen will continue to work with our customers, especially those facing foreclosure, to find loan modification programs, including principal reduction programs, to help them better afford and remain in their homes.”