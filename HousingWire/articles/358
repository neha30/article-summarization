Say what? J.D. Power survey shows customer satisfaction shifting to big banksWhile regulators may worry that the big banks are still too-big-to-fail, the American public opinion of their services continues to improve.

As midsized banks experience their first drop in customer satisfaction since 2010, big banks improved significantly for the sixth year in a row, according to the J.D. Power 2016 U.S. Retail Banking Satisfaction Study.

Some of big banks’ growth in customer satisfaction has been contributed to improved digital offerings, more engaged personal interactions and stronger connections with growth segments of the population.

The survey measures responses from over 75,000 customers pertaining to account information, channel activities, facility fees, problem resolution and product offerings, wherein channel activities includes ATM, branch, call center, IVR, mobile and website.

“Based on their current trajectory, the country’s largest retail banking institutions are expected to achieve a substantial lead in overall customer satisfaction vs. Midsize and Regional banks by 2020,” said Jim Miller, J.D. Power senior director of banking.  “This trend puts Midsize banks most at risk.”

“Regulatory costs have made it difficult for them to invest in strategies to compete with larger rivals, and unless they take proactive steps to change course, we expect this to result in consolidation in the Midsize bank marketplace,” Miller said.

Overall satisfaction with the retail banking industry improved three points since 2015 to 793 on a 1,000 point scale. Satisfaction with big banks also improved six points since 2015 to 793. Midsized banks, on the other hand, decreased by 5 points since last year, but still lead with 797 points. Regional banks remained flat with 790 points. Big bank segment satisfaction improved significantly by 56 index points.

In fact, big banks scored the highest in mobile with 851 points, followed by ATM with 837 points and online satisfaction with 838 points.

Big banks have been especially successful in winning over Millennials, the fastest growing customer segment, the survey showed. Whereas Millennials may be the largest growth potential for retail banks, they also post much higher risk of attrition.

“While customer satisfaction with Big, Midsize and Regional banks falls within a tight 7-point range, establishing customer service tools for competitive differentiation is key to a successful path forward,” said Paul McAdam, J.D. Power senior director of banking services. “We clearly see that the customer satisfaction leaders in retail banking excel by hitting the sweet spot of providing a great digital experience backed by personal service.”

These are the top rated banks by region: