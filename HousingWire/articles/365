Ocwen foreclosures frozen after National Mortgage Settlement compliance failureAs it turns out, it can get worse for Ocwen Financial.

Less than one day after posting a massive loss for the first quarter of 2016, the nonbank has run afoul of the terms of the National Mortgage Settlement and is now forbidden from taking foreclosure actions on more than 17,000 loans.

According to Joseph Smith, the monitor of the National Mortgage Settlement, Ocwen is not yet back in compliance with one of the performance metrics of the National Mortgage Settlement that it failed in the second half of 2014.

Smith’s office announced in October that Ocwen failed metric 31, which tests whether the mortgage servicer, Ocwen in this case, sent a loan modification denial notification to a borrower that included the reason for the denial, the factual information considered by the servicer in making its decision and a timeframe by which the borrower can provide evidence that the decision was made in error.

According to Smith, Ocwen has not fully remedied the issues that led to the compliance failure.

Smith’s office stated that Ocwen “was delayed” in implementing its Corrective Action Plan for the failure of Metric 31 because of “difficulties in resolving the technical issues that led to the original fail.”

And because of those issues, Ocwen must place 17,496 loans that "could have been affected" by this issue on foreclosure hold.

“While Ocwen has made progress toward correcting a number of past fails, it has not resolved its issues that led to its failure of Metric 31,” Smith said.

“Therefore, I will not allow Ocwen to move forward with foreclosures on any borrowers who could have been affected by this failure until each of these borrowers has correct information and a chance to appeal,” Smith continued.

According to Smith’s office, the hold will not be lifted until every borrower who could have been affected gets correct information and a chance to appeal.

Smith’s office stated it recently received and approved Ocwen’s Corrective Action Plan for Metric 31, and that Ocwen has implemented the plan.

Smith said that if his office determines that Ocwen’s CAP has indeed been implemented, testing of the issue will resume in the second quarter of 2016.

“Despite its progress, Ocwen continues to have work to do,” Smith said. “I will continue my efforts to review Ocwen’s compliance with the NMS and resolve any outstanding issues. I will report to the Court and the public on these efforts in the coming months.”

But it’s not all bad news for Ocwen in the eyes of Smith and the NMS.

Smith’s office stated that Ocwen has provided 23,000 borrowers with more than $2 billion in consumer relief, which fulfills its consumer relief obligation under the NMS.

“As a result of my review of the (Ocwen’s internal review group’s) work papers for Ocwen’s claimed consumer relief credit through Sept. 30, 2015, I have credited Ocwen with $1,246,442,217 toward its consumer relief obligation,” Smith said. “In total, I have credited Ocwen with $2,127,661,401 in consumer relief credit and have determined that Ocwen has exceeded its consumer relief obligations.”

Smith also stated that Ocwen did not fail any of the compliance metrics tested during the first half of 2015 and continues to work on corrective action plans designed to fix past fails.

In a statement, Ocwen said the company is pleased with the progress it has made.

“Ocwen is pleased that the Monitor’s second compliance report determined that we have passed all tested metrics in the first and second quarters of 2015,” the company said.

“These results confirm that we have implemented the appropriate actions to be compliant with our obligations under the National Mortgage Settlement,” the company continued.

‘Ocwen has made significant investments in our risk and compliance management infrastructure to ensure that we are fully compliant with all rules and regulations related to our business,” the company said. “We will continue to work closely with the Monitor and look forward to the next report.”

In fact, Smith’s office stated that Ocwen completed its Global Corrective Action Plan, which is a plan to correct a variety of letter-dating issues previously uncovered.

Smith also said that the Corrective Action Plans for Metrics 7, 8, 19 and 23 also are complete.

“The Monitor’s latest Consumer Relief Report is another positive step for Ocwen, and confirms our commitment to providing real solutions to struggling homeowners,” the company said.

“Our work with distressed borrowers will not end just because we have exceeded our NMS obligations,” the company continued.

“Families across the country are still being impacted by the financial crisis,” the company concluded. “Ocwen will continue to work with our customers, especially those facing foreclosure, to find loan modification programs, including principal reduction programs, to help them better afford and remain in their homes.”