How much worse can it get for Ocwen? Nonbank posts massive loss in 1st quarterEarlier this year, Ocwen Financial tried to lay the groundwork for just how poor its first-quarter financial results would be, stating in February that the nonbank expected to post a loss in 2016, but the company’s first-quarter results are even worse than expected.

Ocwen announced Wednesday after the market closed that it posted a net loss of $111.2 million (or $ 0.90 per share) for the first quarter, which was worse than consensus estimates.

The loss marks a stark reversal from the first quarter of 2015, but not a departure from the nonbank’s financial results of late.

Ocwen actually posted profits – albeit small ones – in the first and second quarters of 2015.

In the first quarter of 2015, Ocwen reported net income of $34.4 million, while in the second quarter, Ocwen reported net income of $10 million.

Those profits were undone by Ocwen’s rough third quarter, when the nonbank posted a net loss of $66.8 million.

And the results were far worse for Ocwen in the fourth quarter, when the nonbank posted a net loss of $224.3 million.

For the full year 2015, Ocwen took a net loss of $246.7 million, which followed a loss of $546 million in 2014.

Ocwen’s rough first quarter of 2016 mirrors its previous two quarters, as the company’s revenue was down 35.2% from the first quarter of 2015, falling to $330.8 million from $510.4 million in 2015.

According to Ocwen, the decrease in revenue was “primarily driven” by the impact of sales of agency mortgage servicing rights and portfolio run-off in 2015.

A look at Ocwen’s total servicing portfolio provides an indication of just how much the sale of mortgage servicing rights has affected its business.

At the end of the first quarter of 2015, Ocwen's servicing portfolio including REO was $382.21 billion in terms of unpaid principal balance.

Now, just one year later, Ocwen’s servicing portfolio is $237.08 billion, a decrease of more than $145 billion, or nearly 38%.

Ocwen reported that its pre-tax loss for the first quarter was $102.1 million, with losses of $32.7 million in “unfavorable interest rate driven fair value changes” for its Ginnie Mae, Fannie Mae, and Freddie Mac mortgage servicing rights, and $30 million of monitor costs relating to its various settlements with regulators.

Ocwen stated that its servicing segment recorded a $68.3 million pre-tax loss that includes a loss of $32.7 million in MSR fair value changes.

The company’s Lending segment recorded a $2 million pre-tax gain for the first quarter of 2016 driven by higher lock volumes across the correspondent and direct channels.

Ocwen’s president and CEO, Ron Faris, said that the company is pleased with its cost-cutting efforts, which included the company reducing its expenses by $49.7 million from the first quarter of 2015, but knows the company has more work to do.

“We are pleased to see the progress of our ongoing cost improvement efforts. Company-wide, we saw adjusted operating expenses decline by $55 million or 17% from the prior quarter,” Faris said.

“Excluding MSR fair value changes and monitoring expenses, which we have no or limited ability to control, and our new initiative spending, our servicing and corporate segments reduced expenses by $80 million,” Faris continued.

“We are focused on making further progress on our cost goals while continuing to enhance the borrower experience,” Faris added.

“We also remain committed to investing in our lending businesses, which we believe will drive earnings growth in the future,” Faris concluded. “Unfortunately, $3 million of monitor costs and $33 million in MSR value decline from the drop in interest rates during the quarter negatively impacted the first quarter results.”

Ocwen also reported that the company completed 16,604 loan modifications in the quarter. The company said that 46% of those loan mods were HAMP modifications and 45% of the mods included some form of principal reduction.