Radian 1Q revenue beats expectationsThe mortgage insurer’s earnings per share of $0.37 per share, excluding non-recurring items, were in-line with expectations.

“Our solid first quarter results were driven primarily by exceptional credit trends," said Radian CEO S.A. Ibrahim. “We delivered on our commitment to strengthen our financial position, increase capital flexibility and improve our debt maturity profile, and during the quarter Radian Guaranty returned to investment grade ratings.”

New mortgage insurance written for the first quarter dropped to $8.1 billion, compared to $9.1 billion in the fourth quarter of 2015 and $9.4 billion in the prior-year quarter.

Furthermore, total primary mortgage insurance in force as of March 31, 2016, was $175.4 billion, compared to $175.6 billion as of Dec. 31, 2015, and $172.1 billion as of March 31, 2015.

The mortgage insurance provision for losses continued to dropped, falling to $43.3 million in the first quarter of 2016, compared to $56.8 million in the fourth quarter of 2015, and $45.9 million in the prior-year period.

“The provision for losses in the first quarter included the positive impact of a modest reduction in the company’s default to claim rate assumption for new notices of default as well as positive development on existing defaults,” the earnings report stated.

Other key factors include that the mortgage insurer’s total number of primary delinquent loans decreased by 13% in the first quarter from the fourth quarter of 2015, and by 24% from the first quarter of 2015.

The primary mortgage insurance delinquency rate declined to 3.5% in the first quarter of 2016, compared to 4% in the fourth quarter of 2015, and 4.6% in the first quarter of 2015.

Also falling, total mortgage insurance claims paid were $127.7 million in the first quarter, compared to $176.5 million in the fourth quarter of 2015, and $207.1 million in the first quarter of 2015. The company continues to expect claims paid for the full-year 2016 of approximately $400–450 million.