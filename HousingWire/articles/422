Fannie Mae shifts more credit risk to insurers with latest dealFannie Mae announced Thursday that it completed its latest effort to shift some of its credit risk away from the taxpayers and onto private insurers.

This latest deal was conducted through Fannie Mae’s Credit Insurance Risk Transfer program, which shifts credit risk on a pool of loans to a panel of reinsurers.

The deal helps to further diversify its counterparty exposure and reduce taxpayer risk by increasing the role of private capital in the mortgage market, Fannie Mae said.

The deal, CIRT 2016-3, is Fannie Mae’s 10th Credit Insurance Risk Transfer deal since the program began in 2013.

Furthermore, the deal shifts a portion of the credit risk on a pool of single-family loans with an unpaid principal balance of approximately $5.7 billion to a single insurer, Fannie Mae said.

The covered loan pool consists of 30-year fixed rate loans with loan-to-value ratios greater than 60% and less than or equal to 80%.

Fannie Mae acquired the loans from May 2015 through June 2015.

In this transaction, which became effective March 1, 2016, Fannie Mae retains risk for the first 50 basis points of loss on a $5.7 billion pool of loans.

If this $28.5 million retention layer were exhausted, the insurer would cover the next 250 basis points of loss on the pool, up to a maximum coverage of approximately $142.3 million.

According to Fannie Mae, the coverage is provided based upon actual losses for a term of 10 years.

Fannie Mae said that depending upon the pay down of the insured pool and the amount of insured loans that become seriously delinquent, the aggregate coverage amount may be reduced at the 3-year anniversary and each anniversary of the effective date thereafter.

Fannie Mae may cancel the coverage at any time on or after the 5-year anniversary of the effective date by paying a cancellation fee, the government-sponsored enterprise said.

“We continue to see strong interest from insurers and reinsurers in our CIRT program and look forward to pursuing additional opportunities to transfer risk to these parties in the future,” said Rob Schaefer, vice president for credit enhancement strategy & management, Fannie Mae. “Fannie Mae remains committed to leading efforts to bring private capital into the housing market.”