Is TRID hysteria over? Time to close drops to 12-month lowWell, it’s now been more than six months since the implementation of the Consumer Financial Protection Bureau’s TILA-RESPA Integrated Disclosures rule.

And it appears that, despite the initial hiccups and headaches, lenders now have this whole TRID thing figured out, as the time to close a loan fell to a 12-month low in March.

The positive news comes courtesy of the latest Origination Insight Report from Ellie Mae, which showed that the time to close all loans dropped to an average of 44 days in March.

That’s the shortest time to close since March 2015 and down two days from February, when the time to close fell to 46 days.

And February’s closing time of 46 days was a significant drop from January, when the average time to close was 50 days.

According to Ellie Mae’s origination report, which is pulled from a “robust” sampling of approximately 75% of all mortgage applications that were initiated on the Ellie’ Mae’s Encompass system, the average time to close a purchase decreased from 48 days in February to 45 days in March.

Additionally, the time to close a refinance also decreased from 44 days in February to 41 days in March.

Ellie Mae’s report also showed that the average time to close Federal Housing Administration loans fell to 44 days in March from 47 days in February.

While closing times are shrinking, the average closing rate is rising, rising to 70.6% in March, up from 69.9% in February, which is the highest closing rate since Ellie Mae began monitoring the data in August 2011.

Refinance closing rates increased to 66.2%, while purchase closing rates increased to just over 75%, Ellie Mae’s report showed.

“We continue to see a decrease in days to close from 46 days in February to 44 days in March,” said Jonathan Corr, president and CEO of Ellie Mae.

“In addition, the percentage of loans closing are continuing their upswing, increasing one percentage point to just over 70%, which is the highest closing rate we’ve seen since we began tracking data in August of 2011,” Corr added. “However, we’re still seeing credit remain relatively tight with 67% of closed loans having FICO scores of 700 or above.”

As Corr said, the average FICO score for closed loans remains high.

According to Ellie Mae’s report, 67% of closed loans had FICO scores of 700 or above, while 12% of closed loans had FICO scores of 650 or below.