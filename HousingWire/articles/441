Sindeo joins REach Real Estate accelerator class of 2016Mortgage marketplace company Sindeo was chosen to participate in the eight-month REach program, run by Second Century Ventures, the technology fund and strategic arm of the National Association of Realtors.

REach was created to identify and support new companies that NAR believes will benefit both Realtors and the broader real estate industry. The program provides real estate-focused education, mentorship and market exposure for selected companies. In this case, Sindeo was one of seven companies chosen for the program out of hundreds that applied because of its innovative business model focused on reinventing the mortgage experience, according to a company press release.

“Sindeo is modernizing the mortgage experience, making it simpler and more transparent for consumers and the REALTORS they work with,” said Dale Stinton, president of SCV and NAR CEO. “We are excited to introduce Sindeo to more than a million NAR members while providing the company with access to experts and influencers in the real estate industry to help accelerate their already-impressive growth.”

“We are honored to be accepted into the REach program and look forward to working with Second Century Ventures and the National Association of Realtors," said Sindeo’s Chief Industry Officer, Ginger Wilcox.

“Through this program, we’ll be able to provide NAR members with a better financing option for their clients, ultimately helping them qualify more borrowers and close more transactions. Gaining this recognition from NAR is invaluable to Sindeo, and will fast-track us towards our goal of providing a better path to home ownership for homebuyers nationally,” added Wilcox.

According to the company, Sindeo seeks to provide a new, consumer-centric path to home financing through a broad marketplace of lenders and loan products, guidance from mortgage advisors and innovative technology. The fast-growing startup has increased operations from one state at the beginning of 2015 to currently operating in 11 states.

With the support of the REach program, Sindeo is now seeking to grow its business, with plans to expand operations nationwide by the end of 2016.