Former Lehman Bros. exec claims to be victim of $2M real estate phishing scamA former managing director at Lehman Brothers claims that his real estate attorney’s email address was hacked last year, leading to the theft of nearly $2 million that was earmarked as the deposit for a $20 million apartment in Manhattan, the New York Post reported this week.

According to the New York Post report, Robert Millard, a former Lehman Brothers executive and current head of the private investment firm Realm Partners, and his wife worked with a Long Island lawyer, Patricia Doran, last year on the purchase of a sizable apartment in Manhattan.

But, the Millards’ deposit money was stolen after hackers infiltrated Doran’s “vulnerable” AOL email account, and the Millards blame Doran for the theft.

While in contract, Doran’s email was hacked and the cyber criminals intercepted communications about the purchase, according to the Millards’ Manhattan civil suit. Posing as Doran the hackers sent the Millards wire transfer instructions to a bank that purportedly belonged to the seller. They wired $1.938 million to the TD Bank account.

According to the Post report, TD Bank was able to recover most, but not all, of Millard’s $2 million deposit. There was $200,000 that was TD Bank was unable to recover.

Now, the Millards are suing Doran for the lost $200,000, claiming that Doran didn’t use more stringent security features.

If the Millards’ claims are true, they become the most notable victims of a mortgage phishing scam that the Federal Trade Commission and the National Association of Realtors recently identified and warned consumers about.

According to the FTC and NAR, scammers are hacking the email accounts of consumers and real estate professionals to obtain information about upcoming real estate transactions.

Once they have access to the consumers or real estate professionals’ email account, the hackers figure out the closing dates of the deal.

Then, the scammers send an email to the buyer, posing as the real estate professional or title company, saying there’s been a “last minute change” to the wiring instructions.

The scammers’ email instructs the buyer to sent the funds to a different account, which belongs to the scammers.

And then, the money is gone within a “matter of minutes."