HW Women of Influence™ Program DetailsThis unique annual program – now in its sixth year – was the first such national effort of its kind, launched to recognize the significant contributions of women to both mortgage banking and real estate. HousingWire is honored to have led the way.

The 2016 Women of Influence program will recognize the outstanding efforts of women in driving the U.S. housing economy forward.

The honors are given to individuals who are making notable contributions to both their businesses and to the industry at-large – with a specific focus on contributions made in the most recent 12 months. Their energy, ideas, achievements, as well as commitment to excellence and progress give us a look at the future of the industry.

If you know a woman making such an impact — and it could even be you! — please complete the nomination process.

First and foremost, we are looking for evidence of professional success over the last 12-months. This is not a lifetime achievement program, but we will consider lifetime accomplishments in assessing a nomination. Must work full time within the U.S. housing economy: meaning in the fields of residential mortgage lending, servicing, investments, or real estate.

Cost:

 The 2016 nomination fee is $399, which covers costs associated with editorial review and production, as well as custom awards and photography for this year's honorees.

1. Who decides who will be recognized as a 2016 Women of Influence honoree?

A panel of HousingWire’s editors and reporters will decide.

2. How can I nominate someone?

Nominations must be submitted online, see above link.

3. Why is there a nomination fee?

That's simple – it covers editorial time, as well as the selected honoree package.

Those selected to this year's class of Women of Influence will receive a customized winner's package that includes a gorgeous handmade plaque, recognition in both the August 2016 issue of HousingWire Magazine, and year-long recognition on HousingWire.com.

There are no additional fees, nothing else to buy. It takes a significant amount of time and resources for HousingWire’s editorial team members to organize and successfully determine who will be named to this year’s list.

4. Can I just call you or send you an email with the name of the person I'd like to nominate?

No. We need to have the form filled out, including contact information for the person you are nominating.

5. I want to nominate my boss, but would it be better if someone higher up nominated instead? Can I nominate a family member?

You can nominate yourself, a client, a family member, your boss, an employee, or an acquaintance. The most important thing is that you state a clear, business-oriented reason for the nomination.

6. How does the selection process work?

After we receive a nomination, HousingWire editorial staff members vet the candidates and submit lists to the editors. The editors then hold a series of meetings to determine which candidates will be chosen. We strive to come up with a diverse list that represents a broad spectrum of the industries we cover. The process takes a significant amount of time over a compressed schedule — more than 100 hours in less than three weeks!

7. What is required of those who are selected?

Candidates who are selected must make themselves available for an interview with a reporter. A high resolution headshot might possibly be requested.

8. Can I contact you to see how my nominee is doing in the judging process?

No. We don't give status reports. If we need any further information, we will contact you. Please be patient.

9. I want to be selected for the Women of Influence program. Will it help to hire a public relations firm?

No. Publicists often submit candidates, but they do not influence the judging.

10. I or my nominee was not selected for this year’s class of HousingWire’s Women of Influence. Can you tell me why?

With hundreds of candidates and only a few spots available, the majority of hopefuls will not be chosen. We cannot explain why you weren't picked because it typically won't be for any one reason, but for reasons that have to do with presenting a diverse range of professions and professionals spanning the U.S. housing economy.

Please contact editor@housingwire.com with any questions about this special program.