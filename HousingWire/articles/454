Bernie Sanders announces sweeping housing agendaSay what you will about the Democratic candidates for president (and many do, often with good reason), but at least they are talking housing in 2016.

Earlier this year, former Secretary of State Hillary Clinton released her full economic agenda, which included several major housing reforms like matching down payments for some borrowers in “underserved communities,” increasing funding for housing counseling programs, updating mortgage underwriting tools and working to expand access to credit.

Now, Sen. Bernie Sanders, I-VT, is joining Clinton in releasing his own housing agenda, which Sanders’ campaign announced over the weekend.

In a section on Sanders’ website, posted in conjunction with Sanders’ weekend visit to a New York public housing project, the senator from Vermont states that while the housing market has improved since the crisis, there is still serious work to be done.

“Millions of Americans remain underwater with their mortgages and millions more can’t get a loan to buy a house,” Sanders’ campaign states.

“On the rental side, more than 7 million households lack access to adequate affordable housing, with many facing a daily choice between housing, food, and healthcare,” the message continues.

“Meanwhile, many working families, veterans, the mentally ill and the poor are living in their cars, in homeless shelters, or simply out on the street. That is unacceptable,” Sanders’ message states. “Working together, this is what we need to do to address the affordable housing crisis.”

To that end, Sanders proposes a series of reforms and expansions for several federal housing programs.

Among Sanders’ proposals is a massive expansion of the National Affordable Housing Trust Fund, with Sanders pledging to increase funding for the program, which helps address the shortage of affordable rental housing options, to “at least $5 billion” in order to “construct, preserve, and rehabilitate” at least 3.5 million affordable housing rental units over the next decade.

Sanders also wants to increase the minimum wage to $15 per hour, by the year 2020.

More than that, Sanders proposes a five-point plan to “promote homeownership,” as in Sanders’ eyes, wages in the U.S. have not risen in accordance with housing prices, making the prospect of owning a home “out of reach” for millions of families.

“The housing crisis that began in 2007 wiped out the entire household wealth of millions of families, particularly African-American and Latino families who were disproportionately targeted with subprime mortgages and had most of their wealth in their homes – the net worth of African-American households fell by more than 50% and Latino families by more than 65%,” Sanders states. “We must take aggressive steps to make homeownership more attainable for first-time homebuyers, as well as for the millions of families across the country who lost their homes to foreclosure.”

To that end, Sanders proposes five reforms, including:

Supporting first-time homebuyers by expanding the Department of Housing and Urban Development and USDA Rural Development assistance programs for first-time homeownership, particularly through “down payment assistance, loan guarantees and direct loans.” Expanding pre-purchase housing counseling, as according to Sanders, study after study shows that people who receive counseling before buying a home are more likely to succeed at homeownership. Housing counseling is a good investment in families and communities, Sanders states. Implementing credit score reform. According to Sanders, the credit scores of millions of families were “ruined” because of foreclosures or other financial hardships stemming from the crisis. “At the same time, a prime score before the crisis was 640,” Sanders states. “It currently hovers around 740. If we want to rebuild the lost wealth of working families, we need real credit score reform to make the banking and credit industries work for borrowers and not just lenders.” Preventing predatory lending. According to Sanders, in the 2000s, Sanders called on Congress to clean up the subprime market by cracking down on predatory lenders, but states that “unfortunately, that didn’t happen.” Sanders writes that “incredibly,” Republicans now want to undermine the Consumer Financial Protection Bureau, By requiring that all mortgage costs are clear, risks are visible, and nothing is buried in fine print, the CFPB makes sure consumers have the information they need to make good financial decisions, Sanders states. Protecting homeowner mortgage interest benefits. Sanders writes that he “strongly supports” tax policies that promote homeownership, and opposes any reform that would negatively impact middle and low-income homeowners. “We need to close the second home and yacht loophole, as there is simply no compelling public interest in subsidizing second homes and yachts,” Sanders states. “We also need to expand homeowner mortgage interest benefits to the 19 million otherwise eligible homeowners who do not itemize their taxes.”

Sanders also writes that he wants to do more to help “underwater” borrowers, which are borrowers that owe more on their home than it's worth.

“While the taxpayers of this country, against my strong opposition, bailed out the largest financial institutions in this country with no strings attached, we have never provided an adequate lifeline to underwater homeowners,” Sanders writes.

To aid those underwater borrowers, Sanders proposes the “reinvigoration” of the government’s Home Affordable Refinance Program.

“While the average homeowner saves about $2,500 per year (via HARP), many people who theoretically qualify have not benefitted because of various barriers and inadequate outreach,” Sanders campaign states.

“Sen. Sanders co-sponsored legislation to reduce up-front fees, eliminate appraisal costs for borrowers, streamline the application process and launch a national effort to educate homeowners about the program,” the message continues. “As president, he will fight to sign that legislation into law.”

Sanders also proposes expanding foreclosure mitigation counseling, stating that the “best solution” is keeping homeowners in their homes.