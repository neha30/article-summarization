House Financial Services Committee approves bill to rein in CFPB budgetThe Consumer Financial Protection Bureau, long a target of Republicans on Capitol Hill, especially those on the House Financial Services Committee, is in the crosshairs again, as the House Financial Services Committee approved a bill Wednesday that would bring the CFPB’s budget under Congressional oversight.

Currently, the CFPB is funded by the Federal Reserve instead of via Congressional appropriations, as most other federal agencies are.

A bill approved Wednesday by the full House Financial Services Committee would change that. The bill passed out of the committee by a 33-20 margin.

According to documents published last week by the House Financial Services Committee, the CFPB’s budget is drawn from the Fed “to the extent the Bureau’s Director deems necessary.”

The House Financial Services Committee document notes that the Federal Reserve does not oversee the CFPB or exercise any authority over it, but the Federal Reserve must give the Bureau whatever funds its director requests, up to certain limits.

But the bill, introduced last year by Rep. Andy Barr, R-Kentucky, would take the CFPB’s budget away from the Federal Reserve and bring it to the Congressional appropriations process.

In a statement, House Financial Services Committee Chairman Jeb Hensarling, R-Texas, said that the CFPB’s budget and budgetary process needs to be reined in.

“With respect to the CFPB, we will continue to debate the activities of this particular rogue agency,” Hensarling said.

“But there is a very important principle involved regardless of whether or not we are dealing with the CFPB or any other government agency and that is Congress’ Article I authority to make laws; Congress’ Article I authority to exercise the power of the purse,” Hensarling continued.

“Every government agency should be accountable to the elected representatives of ‘We the People’ and the CFPB should not be an exception to that rule,” Hensarling said.

“We have the Pentagon which is on budget. We have the Justice Department which is on budget,” Hensarling said. “There is certainly no greater duty we have than to provide for the common defense, and we do not let the Pentagon write its own budget. We should not let the CFPB write its own budget.”

According to Hensarling, bringing the CFPB’s budget under Congressional oversight is a “base matter” of Congressional authority.

“And in this particular case, this is an agency that is so deserved of oversight,” Hensarling said. “Merely attending a hearing and sitting in a witness chair is no substitute for actually having the agency on budget.”

In his statement, Hensarling goes on to tell his “friends on the other side of the aisle” that at some point, “the shoe will be on the other foot” when it comes to which party controls the White House.

“At some point the White House will change hands,” Hensarling said. “At some point the Congress will change hands. Do they want a CFPB head who might request $8.47 as the entire budget for the CFPB and have absolutely no voice in the matter? I would have my friends on the other side of the aisle contemplate that.”