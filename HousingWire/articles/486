Will PHH accomplish the unthinkable against the CFPB?Opening arguments are underway in the case of mortgage lender PHH in its challenge against fines levied by the federal regulator, the Consumer Financial Protection Bureau.

And analysts at independent financial firm Compass Point Research & Trading say PHH is definitely favored to win its case against the CFPB.

If so, this represents not only one of the few instances where a mortgage company challenged penalties from the CFPB, but an equally rare case of landing in front of a court that takes its side.

How did PHH, the fifth largest mortgage originator in the nation, accomplish such a feat?

Well, for one, their lawyer took it straight to the top. Instead of defending the mortgage lending activity of PHH, the tactic here is to directly attack the CFPB.

It appears to be working.

In opening arguments, PHH criticized the agency’s structure, authority and even the Director Richard Cordray himself.

Here are some snippets of that argument, though not delivered in this order:

“The director ran roughshod over clear provisions of federal law,” the lawyer for PHH states. “The President and the Congress have no control over this agency. The only check on this agency is right here, if it isn’t for the judiciary, this agency could do anything it wants.”

[If you want to listen to the full audio of the arguments, click here, or here via the court's website.]

Yesterday, in the U.S. Court of Appeals for the District of Columbia Circuit, Judges Kavanaugh and Randolph appeared to greatly sympathize with the PHH beef.

“The CFPB’s argument was met by a clearly unsympathetic bench and our view is the U.S. Court of Appeals for D.C. is set to deliver PHH a victory,” write Compass Point analysts Isaac Boltansky and Fred Small.

“We think there is a 75% probability that the CFPB's $103M increase to the original PHH fine will be substantially reduced or vacated entirely,” they add.

Compass Point is expecting a decision this fall. And if it’s favorable to PHH, it could totally change the game.