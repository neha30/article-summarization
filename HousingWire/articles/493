Independent mortgage bankers see increased profits in 2015Despite losses in 2015’s fourth quarter, Independent mortgage banks and mortgage subsidiaries of chartered banks made about $1,189 on average for each loan they originated in 2015, according to the Mortgage Bankers Association’s Annual Mortgage Bankers Performance Report.

This is up from $747 in 2014, according to the report.

“Despite a drop in profits in the second half of the year compared to the first half, full-year 2015 net production profits were 52 basis points, 18 basis points higher year over year, with higher production volume,” said Marina Walsh, MBA’s vice president of industry analysis. “Profits in 2015 were just below the annual average of 55 basis points since the inception of the Performance Report in 2008.”

“However, because of larger loan balances, per-loan profits were at their third-highest levels since 2008,” Walsh said. “Average loan balances for this sample grew 7% from 2014 to 2015 and have grown 22% since 2008.”

MBA also found that the average production profit was 52 basis points during 2015, up from 34 basis points in 2014. The first half of 2015 started off stronger at 65 basis points, but dropped during the second half to 39 basis points. The average production profit from 2008 to 2015 was 55 basis points.

The average production grew from 1.57 billion per company in 2014 to $2.4 billion per company in 2015. On a repeater company basis, average production volume was even higher at $2.48 billion, up 48% from $1.68 billion in 2014.

Total loan production expenses continued to increase at $7,046 per loan, up from $6,950 in 2014. Once again, the numbers were stronger at the beginning of the year, where production expenses totaled $6,893 per loan, followed by an increase in the second half of the year to $7,272 per loan.

Whereas the net cost to originate was $5,200 in 2014, it increased to $5,567 in 2015. The new cost to originate includes production operating costs and commissions, less all fee income, however it excludes secondary marketing gains, capitalized servicing serving released premiums and warehouse interest spread.

Productivity increased during 2015 to 2.2 loans per production employee. This was up from 2.05 per employee in 2014.

Secondary marketing income plus origination fees also increased in 2015 from 321 basis point in 2014 to 330 basis points.

On the other hand, the purchase share of total originations by dollar volume decreased in 2015 to 64% from 71% in 2014. Including the entire mortgage industry, MBA estimates the purchase shares were at 54%, down from 60% in 2014.

Overall, an estimated 92% of the firms in the study posted pre-tax net financial profits for 2015. This was up from 82% from 2014. This is down from the 93% of firms posting pre-tax financial profits in the first half of 2015, but up from the 83% in the second half.