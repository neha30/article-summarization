Redfin: More homebuyers say Presidential election negatively impacts housing marketThe last major round of primaries is up today, but what does that mean for the housing market? According to a recent special report by Redfin, 27% of homebuyers said this election will negatively affect the housing market.

The study was conducted on nearly 1,000 homebuyers in 36 states and the District of Columbia. Of those, 40% were Millennials and 37% were first-time homebuyers.

Although 27% may not seem significant, it is up from 15% just four months ago, according to the report.

This chart shows what effect homebuyers said the election would have on the housing market compared to the first quarter in 2016:

Also in the report, 10% of respondents said they would consider leaving the country if their preferred candidate lost. Some said they would only consider it, while others said they would absolutely leave.

Interestingly enough, supporters of presidential candidate Bernie Sanders were six times more likely to consider leaving the U.S. than Trump supporters.

So who did the surveyed homebuyers nominate at the best candidate for housing? No one. Actually “Other,” won the most votes at 28%, followed by Bernie Sanders, Hillary Clinton and then Donald Trump.

Here is the breakdown:

“The next president will inherit the lowest homeownership rate in 48 years and so far the voters have heard little to nothing about what the candidates will do to boost people’s chances of becoming homeowners,” Redfin Chief Economist Nela Richardson said. “Candidates need to start discussing housing on the campaign trail now.”

That being said, despite the pessimism surrounding this election, Richardson explained that any effects the next election brings to the market will not be immediate.

“While homeowner anxiety over the election is clearly mounting, the likelihood of an immediate shock to the market is slim,” Richardson said. “It will take considerable time for our next commander-in-chief to implement policies that have any impact on housing.”