This is a python implementation of TextRank for automatic keyword and sentence extraction 


Dependencies
============

Networkx -
  $ sudo pip install networkx

NLTk-
  $ sudo pip install nltk
  $ python
  >>nltk.download('all')

Numpy-
  $ sudo pip install numpy


