Meet the NHL's newest team: the Las Vegas Black Knights?What were merely rumors 18 months ago are now one step from becoming reality, as the National Hockey League’s executive committee reportedly gave its approval to expand the league with a team in Las Vegas led by an ownership group fronted by William Foley, the chairman of Fidelity National Financial and Black Knight Financial Services.

The rumors of Foley’s NHL expansion push first surfaced in November 2014, when multiple outlets, including The Hockey News and Yahoo, reported that the NHL chose the ownership group that featured Foley as well as the Maloof family, who own several Las Vegas hotels and formerly owned the National Basketball Association’s Sacramento Kings.

Now, according to multiple reports, including Canada’s Sportsnet, the nine members of the NHL’s executive committee voted unanimously to approve a Las Vegas expansion team, which is rumored to begin playing in 2017.

According to Sportsnet’s Elliotte Friedman, the next step in the process is for the NHL’s full board of governors to approve the expansion plans, which is expected to happen soon.

The awards show Friedman is referencing is scheduled to take place on June 22, meaning a full vote and subsequent announcement could be coming very soon.

What’s even more interesting about this, especially to those in the mortgage business, is the rumored name of Foley’s team.

According to the Sportsnet report, the Las Vegas expansion team is expected to be called the Las Vegas Black Knights.

As HousingWire wrote in November 2014, Foley’s involvement as an owner of a team in major professional sports team wouldn’t be the first time that a big player in housing sat in the owners’ suite.

Arthur Blank, the owner of the National Football League’s Atlanta Falcons, made his billions as the founder of Home Depot, and Quicken Loans founder and chairman Dan Gilbert is also the owner of the NBA’s Cleveland Cavaliers.

But neither of those teams carry the moniker of their owners’ sources of fortune. It’s not the Atlanta Home Depots or the Cleveland Quickens, although the Cavaliers to play in the Quicken Loans Arena.

But that’s a far cry from the Las Vegas Black Knights.

Although the Black Knights are only the rumored team name, it seems like a likely choice, at least based on the team-run website VegasWantsHockey.com, which was established as part of the expansion effort.

The website serves as a fan-facing platform about the team’s development. The copyright listed on the website is “Hockey Vision Las Vegas,” but the team’s contact information is listed as “Black Knight Sports & Entertainment.”

The expansion efforts won’t be cheap for the Foley/Maloof group either, as Yahoo reports (citing the Associated Press) that the expansion fee for the new Las Vegas team is expected to be $500 million.

The Yahoo report also states that the Las Vegas expansion team is a “done deal.”

When contacted by HousingWire, a spokesperson for Fidelity National and Black Knight said that the company (and Foley) will not be commenting on rumors.

But as Sportsnet and Yahoo state, despite the fact that there are still hurdles left to clear, odds are high that the Las Vegas Black Knights will take the ice next year.

Although it appears that they won’t do it in Black Knight Financial Services Arena. The team’s expected venue opened in April and is called T-Mobile Arena.