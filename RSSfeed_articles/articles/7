FINRA names new CEORecently, the Financial Industry Regulatory Authority announced that its Board of Governors chose Robert Cook as the company’s next president and CEO, effective the second half of 2016.

FINRA provides oversight for broker-dealers that sell securities in the U.S. securities markets.

Cook is replacing Richard Ketchum, who served as chairman and CEO since 2009; In the next few months the Board plans to name a new chairman.

“We thank Rick for his terrific leadership as FINRA’s Chairman and CEO, and express our gratitude on behalf of all investors for his decades of service,” said FINRA Lead Governor Jack Brennan, former Vanguard Group CEO.

Ketchum expressed his approval of his replacement, explaining what Cook will bring to the company.

“Having known and worked alongside Robert for several years, I know that he brings extensive expertise as one of the leading practitioners on broker-dealer and market regulation, as well as proven regulatory and leadership experience,” Ketchum said.

Previously, Cook worked for Cleary Gottieb Steen & Hamilton, where he served as partner in the District of Columbia office since June 2013. There, he focused on the regulation of securities markets and market intermediaries, including broker-dealers, exchanges, alternative trading systems and clearing agencies.

Before that he worked as the director of the Division of Trading and Markets of the U.S. Securities and Exchange Commission since 2010. In this position Cook was responsible for regulatory policy and oversight with respect to broker-dealers, securities exchanges and markets, clearing agengies and FINRA.

Before that, Cook served at Cleary Gottlieb since 1992, and became partner in 2001.

“Robert has a deep understanding of the securities markets, and investors will greatly benefit from his broad regulatory expertise developed as Director of the SEC’s Division of Trading and Markets, where he led the organization in establishing and maintaining standards for fair, orderly and efficient markets,” Brennan said.

Chair Mary Jo White congratulated Cook on his appointment, and his previous accomplishments.

I want to congratulate FINRA and Robert Cook on his selection as President and Chief Executive Officer,” White said. “Robert served the Securities and Exchange Commission with great distinction, advancing a wide range of important initiatives that included significant enhancements to U.S. equity market structure.”

“I have no doubt that Robert will be a tremendous leader for FINRA, and I look forward to working with him to further strengthen our markets and protect investors,” White said.