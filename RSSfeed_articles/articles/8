Stress test bad for lending?The Federal Reserve’s annual stress tests could be causing banks to restrict lending, or so says Bank of America CEO Brian Moynihan, according to a blog by Richard Teitelbaum for The Wall Street Journal.

Under the tests, banks show how they would respond to economic challenges such as a financial shock or a deep recession, the results of which the Fed will share later this month, according to the piece.

“You had to have a conservative nature of everything because at the end of the day, if you think of any business decision you’re going to make, you’d have to assume that the business decision was going to go wrong at the worst possible time and hold capital,” Mr. Moynihan said. “That’s a stress test.”

Moynihan explained that the tests will make banks safer, but may restrict lending as well. After the last round of tests, Bank of America had to resubmit its plan, while others such as JPMorgan Chase and Goldman Sachs adjusted requests to return capital.

In fact, five of the nation’s largest banks are not prepared for a financial crisis and would need taxpayer bailouts, the Board of Governors of the Federal Reserve System and the Board of Directors of the Federal Deposit Insurance Corporation announced in April.

Mr. Moynihan said the company has successfully built its capital. “We worked hard to get all that work done,” he said. “You have a company that’s gone from $100 billion in liquidity to $400 billion in liquidity.” The bank could free up more cash for lending it its capital requirements were lower. “We could lend more money if the capital levels were different,” he said.