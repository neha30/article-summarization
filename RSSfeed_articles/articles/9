April foreclosure inventory down about 25% from last yearForeclosure inventory continues to decline, decreasing 23.4% annually in April, while completed foreclosures declined by 15.8% annually, according to the April 2016 National Foreclosure Report released by CoreLogic, a property information, analytics and data-enabled services provider.

Completed foreclosures averaged 37,000 nationally, down from 43,000 in April 2015. This is down 68.9% from its peak of 117,813 in September 2010.

Whereas foreclosure inventory represents the number of homes at some point in the foreclosure process, completed foreclosures are the homes lost to foreclosure.

"The number of homeowners who have negative equity has fallen by two-thirds since its 2010 peak, and the number of borrowers in foreclosure proceedings has also continued to drop," CoreLogic President and CEO Anand Nallathambi said.

"Despite this progress, about four million homeowners remained underwater at the end of the first quarter, and these borrowers are more vulnerable to foreclosure proceedings if they should fall delinquent," Nallathambi said.

That said, rising home prices caused 268,000 homeowners to regain equity in the first quarter of 2016, pushing the total number of mortgaged residential properties with equity to 92%, according to data from CoreLogic.

Last month, the number of homes in some stage of foreclosure and the number of seriously delinquent mortgages were at levels not seen since late 2007, according to the March 2016 National Foreclosure Report from CoreLogic.

April continued the trend and hit yet another low not seen since 2007, when foreclosures averaged 21,000.

In April, total homes in the national foreclosure inventory included about 406,000 homes, about 1.1% of homes with a mortgage. This is down from April 2015’s 530,000 homes, or 1.4% of all homes with a mortgage.

Loans in serious delinquency, loans that are 90 days or more past due, in foreclosure, or real estate owned, declined by 21.6% annually with 3%, 1.1 million, of mortgages in this category.

"The recovery in home prices and improved labor market have contributed to the drop in seriously delinquent rates," CoreLogic Chief Economist Frank Nothaft said.

"Over the 12 months through April, the CoreLogic Home Price Index for the U.S. rose 6.2% and the labor market gained 2.6 million jobs,” Nothaft said. “We also found that the seriously delinquent rate fell by about three-quarters of a percentage point."