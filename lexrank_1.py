# summarizing the document using Lexrank algorithm. Description of this algorithm is given in "lexrank.pdf" paper.
# Import library essentials

#other things required-
#  1) lexrank.pdf to understand the algorithm(link-http://www.cs.cmu.edu/afs/cs/project/jair/pub/volume22/erkan04a-html/erkan04a.html)
#  2) plain_text.txt file which we are reading in this program

#installation-
#     $ sudo pip install sumy


#.....................................article summarization using lexrank algorithm........................................#

from sumy.parsers.plaintext import PlaintextParser #We're choosing a plaintext parser here, other parsers available for HTML etc.
from sumy.nlp.tokenizers import Tokenizer 
from sumy.summarizers.lex_rank import LexRankSummarizer #We're choosing Lexrank, other algorithms are also built in

file = "plain_text.txt" #name of the plain-text file
parser = PlaintextParser.from_file(file, Tokenizer("english"))
summarizer = LexRankSummarizer()

summary = summarizer(parser.document, 5) #Summarize the document with 5 sentences

for sentence in summary:
    print sentence
