#finding the similarities between the files. finding the synonyms and morphy of words using wordnet.

import io
import itertools
from nltk.corpus import wordnet as wn

def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in itertools.ifilterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element


#open two file containing keywords
fhand1=open("0",'r')
fhand2=open("1",'r')

#store all keyowrds from file into singl list
file1_list=list()
for line in fhand1:
	file1_list.append(line.strip())


file2_list=list()
for line in fhand2:
	file2_list.append(line.strip())


print "intitial content of file1\n",file1_list
#print "\n\n",file2_list

#use for string keywords + synonyms of words + morphy of words 
final1_list=list()
final2_list=list()


for word in file1_list:
	final1_list.append(word)
	final1_list.append(str(wn.morphy(word)))
	syns=wn.synsets(word)
	for i in range(len(syns)):
		final1_list.append(str(syns[i].lemmas()[0].name()))

#extract all the unique words from the tagged
unique_word_set1 =unique_everseen([x for x in final1_list])
word_set1 = list(unique_word_set1)

print "\n\nfinal content of file 1\n",word_set1

print "\n\nintitial content of file2\n",file2_list
#print "\n\n",file2_list

for word in file2_list:
	final2_list.append(word)
	final2_list.append(str(wn.morphy(word)))
	syns=wn.synsets(word)
	for i in range(len(syns)):
		final2_list.append(str(syns[i].lemmas()[0].name()))

#extract all the unique words from the tagged
unique_word_set2 =unique_everseen([x for x in final2_list])
word_set2 = list(unique_word_set2)

print "\n\nfinal content of file 1\n",word_set2

#finding the common words between this two files
result=[val for val in final1_list if val in final2_list]
print "\n\nresult:\n",len(result)

