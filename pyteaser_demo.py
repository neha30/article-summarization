#Program for article summarization using python library pyteaser
#Summaries are created by ranking sentences in a news article according to how relevant they are to the entire text. The top 5 sentences are used to form a "summary". Each sentence is ranked by using four criteria:
#    Relevance to the title
#    Relevance to keywords in the article
#    Position of the sentence
#    Length of the sentence

# installation-
#   $ sudo pip install pyteaser

#github link-https://github.com/xiaoxu193/PyTeaser

#running the program-
# 1 clone the repository
# 2 place this program in this repository
# 3 and then run 
#       $python pyteaser_demo.py

#.......................................article summarization using pyteaser................................................#



from pyteaser import SummarizeUrl
from pprint import pprint
urls = (u'http://www.huffingtonpost.com/2013/11/22/twitter-forward-secrecy_n_4326599.html',
        u'http://www.bbc.co.uk/news/world-europe-30035666',
        u'http://www.bbc.co.uk/news/magazine-29631332')

for url in urls:
    print "\nsummary of article:"
    summaries = SummarizeUrl(url)
    pprint(summaries)
